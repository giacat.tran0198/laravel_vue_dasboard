const decode = (payload) => JSON.parse(atob(payload));

const isPayload = (token) => {
    const payload = token.split('.')[1]
    return decode(payload)
}

const isValid = (token) => {
    const payload = isPayload(token)
    if (payload) {
        return _.includes(payload.iss, '/api/auth/login') || _.includes(payload.iss, '/api/auth/register')
    }
    return false
}

export default {isValid}
