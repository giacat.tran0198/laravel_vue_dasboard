
export const routes = [
    {
        name: "/",
        path: "/",
        component: () => import("./components/auth/login")
    },
    {
        name: "register",
        path: "/register",
        component: () => import("./components/auth/register")
    },
    {
        name: "logout",
        path: "/logout",
        component: () => import("./components/auth/logout")
    },
    {
        name: "forget",
        path: "/forget",
        component: () => import("./components/auth/forget")
    },
    {
        name: "home",
        path: "/home",
        component: () => import("./components/Home")
    },

    // Employee
    {
        name: "store-employee",
        path: "/store-employee",
        component: () => import("./components/employee/create")
    },
    {
        name: "employees",
        path: "/employees",
        component: () => import("./components/employee/index")
    },
    {
        name: "edit-employee",
        path: "/edit-employee/:id",
        component: () => import("./components/employee/edit")
    },

    // Supplier
    {
        name: "store-supplier",
        path: "/store-supplier",
        component: () => import("./components/supplier/create")
    },
    {
        name: "suppliers",
        path: "/suppliers",
        component: () => import("./components/supplier/index")
    },
    {
        name: "edit-supplier",
        path: "/edit-supplier/:id",
        component: () => import("./components/supplier/edit")
    },

    // Category
    {
        name: "store-category",
        path: "/store-category",
        component: () => import("./components/category/create")
    },
    {
        name: "categories",
        path: "/categories",
        component: () => import("./components/category/index")
    },
    {
        name: "edit-category",
        path: "/edit-category/:id",
        component: () => import("./components/category/edit")
    },

    // Product
    {
        name: "store-product",
        path: "/store-product",
        component: () => import("./components/product/create")
    },
    {
        name: "products",
        path: "/products",
        component: () => import("./components/product/index")
    },
    {
        name: "edit-product",
        path: "/edit-product/:id",
        component: () => import("./components/product/edit")
    },

    // Expense
    {
        name: "store-expense",
        path: "/store-expense",
        component: () => import("./components/expense/create")
    },
    {
        name: "expenses",
        path: "/expenses",
        component: () => import("./components/expense/index")
    },
    {
        name: "edit-expense",
        path: "/edit-expense/:id",
        component: () => import("./components/expense/edit")
    },
];
