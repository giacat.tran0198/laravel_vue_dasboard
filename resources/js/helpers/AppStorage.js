const storeToken = (token) => {
    sessionStorage.setItem('token', token);
}

const storeUser = (user) => {
    sessionStorage.setItem('user', user);
}

const store = (token, user) => {
    storeToken(token);
    storeUser(user);
}

const clear = () => {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('user');
}

const getToken = () => sessionStorage.getItem('token');

const getUser = () => sessionStorage.getItem('user');

export default {
    store,
    clear,
    getToken,
    getUser,
}
