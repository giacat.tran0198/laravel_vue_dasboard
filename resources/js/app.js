
require('./bootstrap');

import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

/**
 * Import helper
 */
import User from "./helpers/User";
import NotificationHelper from "./helpers/Notification";
window.User = User;
window.NotificationHelper = NotificationHelper;

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
});
window.Toast = Toast;

// Router Imported
import {routes} from './routes';
const router = new VueRouter({
    routes,
    mode: 'history',
})

const app = new Vue({
    el: '#app',
    router
});
