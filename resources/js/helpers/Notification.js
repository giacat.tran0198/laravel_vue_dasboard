const success = () => {
    new Noty({
        type: 'success',
        layout: 'topRight',
        text: 'Successfully Done!',
        timeout: 1000,
    }).show();
}

const alert = () => {
    new Noty({
        type: 'alert',
        layout: 'topRight',
        text: 'Are you Sure?',
        timeout: 1000,
    }).show();
}


const error = () => {
    new Noty({
        type: 'alert',
        layout: 'topRight',
        text: 'Something Went Wrong ! ',
        timeout: 1000,
    }).show();
}


const warning = () => {
    new Noty({
        type: 'warning',
        layout: 'topRight',
        text: 'Opps Wrong ',
        timeout: 1000,
    }).show();
}


const image_validation = () => {
    new Noty({
        type: 'error',
        layout: 'topRight',
        text: 'Upload Image less then 1MB ',
        timeout: 1000,
    }).show();
}


const cart_success = () => {
    new Noty({
        type: 'success',
        layout: 'topRight',
        text: 'Successfully Add to Cart!',
        timeout: 1000,
    }).show();
}


const cart_delete = () => {
    new Noty({
        type: 'success',
        layout: 'topRight',
        text: 'Successfully Deleted!',
        timeout: 1000,
    }).show();
}

export default {
    success,
    alert,
    error,
    warning,
    image_validation,
    cart_delete,
    cart_success,
}
