import Token from './Token'
import AppStorage from './AppStorage'

const responseAfterLogin = (res) => {
    const access_token = res.data.access_token;
    const username = res.data.name;
    if (Token.isValid(access_token)) {
        AppStorage.store(access_token, username);
    }
}


const hasToken = () => !_.isNil(AppStorage.getToken()) && Token.isValid(AppStorage.getToken());

const loggedIn = () => hasToken();
const logOut = () => AppStorage.clear();

const name = () => loggedIn() ? AppStorage.getUser() : null

const id = () => {
    if (loggedIn()) {
        const payload = Token.payload(Token.getToken());
        return payload.sub;
    }
    return false;
}


export default {
    responseAfterLogin,
    loggedIn,
    logOut,
    name,
    id,
}
